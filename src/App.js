import React from 'react';
import './App.css';
import _ from 'lodash';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      x: 0,
      y: 0,
      innactive: 0,
      start: Date.now(),
      // Is the screeensaver activated
      screensaver: true,
      // How long until the screensaver should activate
      timeout: process.env.REACT_APP_SS_TIMEOUT,
    };
    this.handleTimelineMove = this.handleTimelineMove.bind(this);
    this.handleTimeoutReset = this.handleTimeoutReset.bind(this);
    this.tick = this.tick.bind(this);
    this.tick2 = this.tick2.bind(this);
    this.screenSaverStyle = this.screenSaverStyle.bind(this);
  }

  // Set the touch position in state when the timeline is touched
  handleTimelineMove(e) {
    e.preventDefault();
    let xVal = 0;
    if (e.pageX) {
      xVal = e.pageX;
    }
    if (e.touches && e.touches[0].pageX) {
      xVal = e.touches[0].pageX;
    }
    this.setState({ x: xVal });
  }

  // Clear the screensaver when someone touches the screen
  handleTimeoutReset() {
    this.setState({
      start: Date.now(),
      innactive: 0,
      screensaver: false,
    });
  }

  // Screensaver logic that is run by component timer
  tick() {
    const { innactive } = this.state;
    const screensaver = innactive > this.state.timeout ? true : false;
    this.setState({
      screensaver: screensaver,
      innactive: new Date() - this.state.start
    });
  }

  // Randomly move the slide to a position on the timeline
  // while in screensaver mode
  tick2() {
    this.state.screensaver
    ? this.setState({ x: _.random(200, 1720) })
    : console.log('no ss');
  }

  // Fade the screensaver in or out as it is hidden and displayed
  screenSaverStyle() {
    const { screensaver } = this.state;
    const style = screensaver
                  ? {
        opacity: 1,
      }
                  : {
        opacity: 0
      };
    return style;
  }

  // Set a timer for screensaver that ticks every second
  // Set secondary tick for screensaver internal change every X seconds
  // defined in env vars
  componentDidMount() {
    this.timer = setInterval(this.tick, 1000);
    setInterval(this.tick2, process.env.REACT_APP_SS_CHANGE_INTERVAL);

  }

  render() {
    const screenWidth = 1920;
    const timelineWidth = screenWidth - 200;
    const segments = 21;
    const { x } = this.state;

    // Padding for the left edge of the timeline position
    const timelineX = x < 100 ? 0 : x - 100;

    // Break X position into segments coresponding
    // with the number of map images
    const segment = parseInt(((timelineX / timelineWidth) * segments), 10);

    // Set the map image background to the correct segment
    // Handle the far right edge of the map with a max value
    const backgroundPostionX =
      (-screenWidth * segment) > -38400
      ? (-screenWidth * segment)
      : -38400;
    const mapStyle = {
      backgroundPositionX: backgroundPostionX,
    };

    // Indicator (button) styling
    // Prevent the indicator from running off the timeline
    let indicatorWidth = 120;
    let indicatorX = (x - (indicatorWidth / 2));
    if (x < (80 + indicatorWidth / 2)) {
      indicatorX = 80;
    }
    if (x > (timelineWidth + 120 - (indicatorWidth / 2))) {
      indicatorX = timelineWidth + 120 - indicatorWidth;
    }
    const timelineIndicatorStyle = {
      left: indicatorX,
      width: indicatorWidth,
    };

    return (
      <div
        onMouseMove={this.handleTimeoutReset}
        onTouchMove={this.handleTimeoutReset}
        className="app"
      >
        <div style={this.screenSaverStyle()} className="screensaver-message">
          <img src="screensaver-title.png" alt="Cherokee Lands" />
          {/*<span className="title">Cherokee Lands Taken by Treaties</span>*/}
          {/*<hr />*/}
          {/*<span className="instructions">*/}
          {/*Drag slider<br />*/}
          {/*to see the impact<br />*/}
          {/*of treaties*/}
          {/*</span>*/}
        </div>

        {/* Map graphic, where style controls the displayed segment */}
        <div
          style={mapStyle} className="map-container"
        >
          &nbsp;
        </div>

        {/* Timeline for controling map. Touching or dragging */}
        {/* anywhere along the timeline will adjust the map */}

        <div
          onMouseDown={this.handleTimelineMove}
          onTouchStart={this.handleTimelineMove}
          onMouseMove={this.handleTimelineMove}
          onTouchMove={this.handleTimelineMove}
          className="timeline"
        >
          <div
            style={timelineIndicatorStyle} className="timeline-indicator"
          >
            &nbsp;
          </div>
        </div>
      </div>
    );
  }
}

export default App;
