const express = require('express');
const app = express();

app.use(express.static('build'));

app.listen(5021, () => console.log('Treaty app listening on port 5021!'));
